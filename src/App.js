import React from 'react';
import Routes from '../src/Routes';
import { withRouter } from 'react-router-dom';
import * as actions from '../src/store/actions/index';
import { connect } from 'react-redux';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.props.onAutoSignup();
  }
  render() {
    return <Routes />;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAutoSignup: () => dispatch(actions.authCheckState()),
  };
};

export default withRouter(connect(null, mapDispatchToProps)(App));
