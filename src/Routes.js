import React, { Component } from 'react';
import { Route, withRouter, Switch, Router } from 'react-router-dom';
import Layout from '../src/layout/Layout';
import { connect } from 'react-redux';
import Login from '../src/pages/Login';
import Register from '../src/pages/Register';
import Logout from '../src/pages/Logout';
import Clients from '../src/pages/Clients';
import AddClients from '../src/pages/AddClients';
import * as actions from '../src/store/actions/index';

export class Routes extends Component {
  constructor(props) {
    super(props);
    this.props.onAutoSignup();
  }

  render() {
    return (
      <Router history={this.props.history}>
        <Route path='/'>
          <Layout isAuthenticated={this.props.Authenticated}>
            <Switch>
              <Route path='/' exact component={Login} />
              <Route path='/logout' exact component={Logout} />
              <Route path='/register' exact component={Register} />
              <Route path='/clients' exact component={Clients} />
              <Route path='/addclients' exact component={AddClients} />
            </Switch>
          </Layout>
        </Route>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    Authenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAutoSignup: () => dispatch(actions.authCheckState()),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Routes));
