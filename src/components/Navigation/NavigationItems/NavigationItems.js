import React from 'react';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';

function NavigationItems(props) {
  console.log(props.isAuthenticated);
  return (
    <Nav className='ml-auto'>
      {props.isAuthenticated ? (
        <Link className='nav-link' to={'/addclients'}>
          Ajouer un client
        </Link>
      ) : null}
      {props.isAuthenticated ? (
        <Link className='nav-link' to={'/clients'}>
          Clients
        </Link>
      ) : null}
      {props.isAuthenticated ? (
        <Link className='nav-link' to={'/logout'}>
          Se déconnecter
        </Link>
      ) : (
        <Link className='nav-link' to={'/'}>
          Connexion
        </Link>
      )}
    </Nav>
  );
}

export default NavigationItems;
