import React from 'react';
import { Link } from 'react-router-dom';

const Button = (props) => {
  return (
    <Link to={props.href} className={props.className} onClick={props.onClick}>
      {props.text}
    </Link>
  );
};

export default Button;
