import React from 'react';

const Client = (props) => {
  return (
    <tr id={props.id}>
      <td>{props.firstName}</td>
      <td>{props.lastName}</td>
    </tr>
  );
};

export default Client;
