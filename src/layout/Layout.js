import React, { Component } from 'react';
import Hoc from '../Hoc/Hoc';
import ToolBar from '../components/Navigation/Toolbar/ToolBar';

class Layout extends Component {
  render() {
    return (
      <Hoc>
        <ToolBar isAuth={this.props.isAuthenticated}></ToolBar>
        <main className={'Content'}>{this.props.children}</main>
      </Hoc>
    );
  }
}

export default Layout;
