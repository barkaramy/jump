import React, { Component } from 'react';
import Input from '../components/UI/Input/Input';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Button from '../components/UI/Button/Button';
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import Spinner from '../components/UI/Spinner/Spinner';
import { Redirect } from 'react-router-dom';

class AddClients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      controls: {
        firstName: {
          elementType: 'input',
          elementConfig: {
            type: 'text',
            placeholder: 'Nom',
          },
          value: '',
          label: 'Nom :',
          validation: {
            required: true,
          },
          valid: false,
          touched: false,
        },
        lastName: {
          elementType: 'input',
          elementConfig: {
            type: 'text',
            placeholder: 'Prénom',
          },
          value: '',
          label: 'Prénom :',
          validation: {
            required: true,
          },
          valid: false,
          touched: false,
        },
        streetNumber: {
          elementType: 'input',
          elementConfig: {
            type: 'number',
            placeholder: 'Numéro de rue',
          },
          value: '',
          label: 'Numéro de rue :',
          validation: {
            required: true,
          },
          valid: false,
          touched: false,
        },
        streetAdress: {
          elementType: 'input',
          elementConfig: {
            type: 'text',
            placeholder: 'Rue',
          },
          value: '',
          label: 'Rue :',
          validation: {
            required: true,
          },
          valid: false,
          touched: false,
        },
        city: {
          elementType: 'input',
          elementConfig: {
            type: 'text',
            placeholder: 'Ville',
          },
          value: '',
          label: 'Ville :',
          validation: {
            required: true,
          },
          valid: false,
          touched: false,
        },
        country: {
          elementType: 'input',
          elementConfig: {
            type: 'text',
            placeholder: 'Pays',
          },
          value: '',
          label: 'Pays :',
          validation: {
            required: true,
          },
          valid: false,
          touched: false,
        },
        postalCode: {
          elementType: 'input',
          elementConfig: {
            type: 'number',
            placeholder: 'Code postal',
          },
          value: '',
          label: 'Code postal :',
          validation: {
            required: true,
          },
          valid: false,
          touched: false,
        },
      },
    };
  }

  checkValidity(value, rules) {
    let isValid = true;
    if (!rules) {
      return true;
    }

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isNumeric) {
      const pattern = /^\d+$/;
      isValid = pattern.test(value) && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event, controlName) => {
    const updatedControls = {
      ...this.state.controls,
      [controlName]: {
        ...this.state.controls[controlName],
        value: event.target.value,
        valid: this.checkValidity(
          event.target.value,
          this.state.controls[controlName].validation
        ),
        touched: true,
      },
    };
    this.setState({ controls: updatedControls });
  };

  addClient = (e) => {
    e.preventDefault();
    this.props.onAddClient(
      this.props.userToken,
      this.state.controls.firstName.value,
      this.state.controls.lastName.value,
      this.state.controls.streetNumber.value,
      this.state.controls.streetAdress.value,
      this.state.controls.city.value,
      this.state.controls.country.value,
      this.state.controls.postalCode.value
    );
  };

  render() {
    const formElementsArray = [];
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key],
      });
    }

    let form = formElementsArray.map((formElement) => (
      <Input
        key={formElement.id}
        className='input-register-container'
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        label={formElement.config.label}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        changed={(event) => this.inputChangedHandler(event, formElement.id)}
      />
    ));

    if (this.props.loading) {
      form = <Spinner />;
    }
    const addedRedirection =
      this.props.response === 201 ? <Redirect to={'/clients'} /> : null;

    return (
      <Container fluid={true} className='bg-light p-5 h-100'>
        <Row className='justify-content-center'>
          {addedRedirection}
          <Row
            className='d-flex flex-row justify-content-between p-20 form-register-container'
            md={12}>
            {form}
            <div className='d-flex flex-column justify-content-center w-100'>
              <Button
                className='btn'
                text='Ajouter un client'
                onClick={this.addClient}
              />
            </div>
          </Row>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.client.loading,
    error: state.client.error,
    userToken: state.auth.token,
    response: state.client.response,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddClient: (
      token,
      firstName,
      lastName,
      streetNumber,
      streetAdress,
      city,
      country,
      postalCode
    ) =>
      dispatch(
        actions.addClients(
          token,
          firstName,
          lastName,
          streetNumber,
          streetAdress,
          city,
          country,
          postalCode
        )
      ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddClients);
