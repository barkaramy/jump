import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class Clients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clientDetails: {},
      isActive: null,
      clicked: false,
    };

    this.props.onFatchClients(this.props.userToken);
  }

  handleClientDetails = (id) => {
    this.setState({ clicked: !this.state.clicked });

    console.log(id);
    let Client = this.props.clients.filter((client) => client._id === id);
    console.log(Client);
    console.log(id);
    if (id === this.state.isActive) {
      this.setState({
        isActive: null,
      });
    } else {
      this.setState({
        isActive: id,
      });
    }
    this.setState({ clientDetails: Client });
  };

  render() {
    let clients = this.props.clients.map((client) => {
      return (
        <tr
          style={{
            backgroundColor:
              this.state.isActive === client._id ? 'rgba(0,0,0,0.05)' : '',
          }}
          key={client._id}
          onClick={() => this.handleClientDetails(client._id)}>
          <td>{client.firstName}</td>
          <td>{client.lastName}</td>
        </tr>
      );
    });
    console.log('clientdetails', this.state.clientDetails);

    let clientAdress =
      this.state.clientDetails.length > 0
        ? this.state.clientDetails.map((d) => {
            return (
              <p>
                {d.adress.streetNumber},{' ' + d.adress.streetAdress},
                {' ' + d.adress.postalCode},{' ' + d.adress.city},
                {' ' + d.adress.country}
              </p>
            );
          })
        : null;

    return (
      <Container fluid={true} className='bg-light h-100  p-5'>
        <Row className='justify-content-center mx-5 mt-5 p-5 bg-white'>
          <Col md={8} className='table'>
            <Table bordered hover>
              <tbody>
                <tr>
                  <th>Nom</th>
                  <th>Prénom</th>
                </tr>
                {clients}
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row className='justify-content-center mx-5 p-5 bg-white'>
          <Col md={6}>
            <fieldset className='details-fieldset'>
              <legend className='details-legend'>Details</legend>
              {clientAdress}
            </fieldset>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userToken: state.auth.token,
    clients: state.client.clients,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFatchClients: (token) => {
      dispatch(actions.fetchClients(token));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Clients);
