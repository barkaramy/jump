import axios from 'axios';
import * as actionTypes from './actionTypes';

export const addClientsStart = () => {
  return {
    type: actionTypes.ADD_CLIENTS_START,
  };
};

export const addClientsSuccess = (response) => {
  return {
    type: actionTypes.ADD_CLIENTS_SUCCESS,
    loading: true,
    response: response,
  };
};

export const addClientsEnd = () => {
  return {
    type: actionTypes.ADD_CLIENTS_END,
  };
};

export const addClientsFail = (error) => {
  return {
    type: actionTypes.ADD_CLIENTS_FAIL,
    error: error,
  };
};

export const addClients = (
  token,
  firstName,
  lastName,
  streetNumber,
  streetAdress,
  city,
  country,
  postalCode
) => {
  return (dispatch) => {
    dispatch(addClientsStart());
    const addClientData = {
      token: token,
      firstName: firstName,
      lastName: lastName,
      adress: {
        streetNumber: streetNumber,
        streetAdress: streetAdress,
        city: city,
        country: country,
        postalCode: postalCode,
      },
    };
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    let url = 'http://localhost:3001/clients';
    axios
      .post(url, addClientData, config)
      .then((response) => {
        console.log(response);
        dispatch(addClientsSuccess(response.status));
        dispatch(addClientsEnd());
      })
      .catch((err) => {
        dispatch(addClientsFail(err.response));
      });
  };
};

export const fetchClientsStart = () => {
  return {
    type: actionTypes.FETCH_CLIENTS_START,
  };
};

export const fetchClientsSuccess = (clients) => {
  return {
    type: actionTypes.FETCH_CLIENTS_SUCCESS,
    clients: clients,
  };
};

export const fetchClientsFail = (error) => {
  return {
    type: actionTypes.FETCH_CLIENTS_FAIL,
    error: error,
  };
};

export const fetchClients = (token) => {
  return (dispatch) => {
    dispatch(fetchClientsStart());
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    axios
      .get('http://localhost:3001/clients/all', config)
      .then((res) => {
        dispatch(fetchClientsSuccess(res.data.clients));
      })
      .catch((err) => {
        dispatch(fetchClientsFail(err));
      });
  };
};
