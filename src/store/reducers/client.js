import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  clients: [],
  response: null,
  clientId: null,
  loading: false,
  addedProject: false,
};

const addClientsStart = (state, action) => {
  return updateObject(state, { error: null, loading: true });
};

const addClientsSuccess = (state, action) => {
  return updateObject(state, {
    loading: false,
    response: action.response,
  });
};

const addClientsEnd = (state, action) => {
  return updateObject(state, {
    response: false,
  });
};

const addClientsFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const fetchClientsStart = (state, action) => {
  return updateObject(state, { loading: true });
};

const fetchClientsSuccess = (state, action) => {
  return updateObject(state, {
    clients: action.clients,
    loading: false,
  });
};

const fetchClientsFail = (state, action) => {
  return updateObject(state, { loading: false });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_CLIENTS_START:
      return addClientsStart(state, action);
    case actionTypes.ADD_CLIENTS_SUCCESS:
      return addClientsSuccess(state, action);
    case actionTypes.ADD_CLIENTS_END:
      return addClientsEnd(state, action);
    case actionTypes.ADD_CLIENTS_FAIL:
      return addClientsFail(state, action);
    case actionTypes.FETCH_CLIENTS_START:
      return fetchClientsStart(state, action);
    case actionTypes.FETCH_CLIENTS_SUCCESS:
      return fetchClientsSuccess(state, action);
    case actionTypes.FETCH_CLIENTS_FAIL:
      return fetchClientsFail(state, action);
    default:
      return state;
  }
};

export default reducer;
